<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page import="ua.ithillel.nichushkin.util.NoteUtils" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="ua.ithillel.nichushkin.model.Note" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Все записки</title>

</head>
<body>

<h2>All Notes</h2>
<%--<%--%>
<%--    List<Note> notes = (List<Note>) request.getAttribute("notes");--%>
<%--    Note note1 = notes.get(0);--%>
<%--    response.getWriter().write(note1.getTitle());--%>
<%--    response.getWriter().write(note1.getDescription());--%>
<%--%>--%>


<jsp:useBean id="notes" scope="request" type="ua.ithillel.nichushkin.model.Note"/>
<c:forEach var="note" items="${requestScope.get(notes)}">

    <ul>
        Название: <c:out value="${note.getTitle}"/> <br>
        Описание: <c:out value="${note.getDescription}"/><br>


        <form method="get" action="'/update_note'">
            <input type="number" hidden name="id" value="note.id"/>
            <input type="submit" value="Редактировать"/>
        </form>

        <form method="post" action="'/delete_note'">
            <input type="number" hidden name="id" value="note.id"/>
            <input type="submit" name="delete" value="Удалить"/>
        </form>


    </ul>


</c:forEach>

<h2>Создание новой заметки</h2>
<form method="post" action="'/add_note'/">
    <label>Название <input type="text" name="title"> </label><br>
    <label>Опсисание  <input type="text" name="description"> </label><br>
    <input type="submit" value="OK" name="OK"> <br>

</form>

</body>
</html>
