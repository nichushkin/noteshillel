package ua.ithillel.nichushkin.servlet;

import ua.ithillel.nichushkin.model.Note;
import ua.ithillel.nichushkin.service.Authentication;
import ua.ithillel.nichushkin.service.AuthenticationService;
import ua.ithillel.nichushkin.util.NoteUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@WebListener
public class ContextListener implements ServletContextListener {
    private Map<Integer, Note> notes;
    private AtomicInteger idCounter;

    private Authentication authService;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        final ServletContext servletContext = servletContextEvent.getServletContext();
        notes = new ConcurrentHashMap<>();
        idCounter = new AtomicInteger(3);
        authService = new AuthenticationService();
        servletContext.setAttribute("notes", notes);
        servletContext.setAttribute("idCounter", idCounter);
        servletContext.setAttribute("authService", authService);

        List<Note> noteList = NoteUtils.generateNotes();
        noteList.forEach(note -> this.notes.put(note.getId(), note));

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        notes = null;
    }
}
