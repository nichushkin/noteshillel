package ua.ithillel.nichushkin.servlet;

import ua.ithillel.nichushkin.service.Authentication;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private Authentication authService;

    @Override
    public void init() throws ServletException {
        this.authService = (Authentication) getServletContext().getAttribute("authService");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/view/LoginPage.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String username = req.getParameter("username");
        final String password = req.getParameter("password");

        if (authService.isAuthenticated(username, password)) {
            req.getSession();
            resp.sendRedirect("/notesmenu");
        } else {
            resp.getWriter().write("Wrong login or password");
        }
    }
}
