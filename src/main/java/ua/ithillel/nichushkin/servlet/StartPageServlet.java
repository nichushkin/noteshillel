package ua.ithillel.nichushkin.servlet;

import ua.ithillel.nichushkin.model.Note;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet
public class StartPageServlet extends HttpServlet {
    private Map<Integer, Note> notes;

    @Override
    public void init() throws ServletException {
        final Object notes = getServletContext().getAttribute("notes");

        if (!(notes instanceof ConcurrentHashMap<?, ?>)) {
            throw new IllegalStateException("Your repo doesn`t initialize");
        } else {
            this.notes = (ConcurrentHashMap<Integer, Note>) notes;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("notes", notes.values());
        request.getRequestDispatcher("/WEB-INF/view/startPage.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
