package ua.ithillel.nichushkin.servlet;

import ua.ithillel.nichushkin.model.Note;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "jspServlet", value = "/jspServlet")
public class jspServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Note> notes = new ArrayList<Note>() {{
            add(new Note(1, "Some title 1", "Some description 1"));
            add(new Note(2, "Some title 2", "Some description 2"));
        }};
        request.setAttribute("notes", notes);
        getServletContext().getRequestDispatcher("/startPage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
