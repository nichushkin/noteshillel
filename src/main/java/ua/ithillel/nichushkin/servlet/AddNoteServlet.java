package ua.ithillel.nichushkin.servlet;

import ua.ithillel.nichushkin.model.Note;
import ua.ithillel.nichushkin.util.NoteUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class AddNoteServlet extends HttpServlet {
    private Map<Integer, Note> notes;

    private AtomicInteger idCounter;

    @Override
    public void init() throws ServletException {
        final Object notes = getServletContext().getAttribute("notes");
        if (!(notes instanceof ConcurrentHashMap<?, ?>)) {
            throw new IllegalStateException("Your repo does not initialize!");
        } else {
            this.notes = (ConcurrentHashMap<Integer, Note>) notes;
        }

        idCounter = (AtomicInteger) getServletContext().getAttribute("idCounter");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");


        final String title = req.getParameter("title");
        final String description = req.getParameter("description");

        final Note note = new Note();
        final int id = this.idCounter.getAndIncrement();
        note.setId(id);
        note.setTitle(title);
        note.getDescription(description);

        notes.put(id, note);


        resp.sendRedirect(req.getContextPath() + "/");
    }

}
