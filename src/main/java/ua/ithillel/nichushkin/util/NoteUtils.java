package ua.ithillel.nichushkin.util;

import ua.ithillel.nichushkin.model.Note;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class NoteUtils {
    public static List<Note> generateNotes() {
        final Note note1 = new Note(1, "Some title 1", "Some description 1");
        final Note note2 = new Note(2, "Some title 2", "Some description 2");

        List<Note> notes = new ArrayList<>();
        notes.add(note1);
        notes.add(note2);

        return notes;
    }

    public static boolean idIsNumber(HttpServletRequest request) {
        final String id = request.getParameter("id");
        return id != null && (id.length() > 0);
    }


}
