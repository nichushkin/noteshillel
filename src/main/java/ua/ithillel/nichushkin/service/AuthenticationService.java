package ua.ithillel.nichushkin.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AuthenticationService implements Authentication{

    private Map<String,String> userProfileData = new ConcurrentHashMap<>();

    {
        userProfileData.put("admin", "qwe");
    }


    @Override
    public boolean isUsernamePresent(String username) {
        return userProfileData.containsKey(username);
    }

    @Override
    public boolean isAuthenticated(String username, String password) {
        if (!isUsernamePresent(username)) return false;
        return userProfileData.get(username).equals(password);
    }

    public AuthenticationService() {
    }

    public AuthenticationService(Map<String, String> userProfileData) {
        this.userProfileData = userProfileData;
    }

    public Map<String, String> getUserProfileData() {
        return userProfileData;
    }

    public void setUserProfileData(Map<String, String> userProfileData) {
        this.userProfileData = userProfileData;
    }
}
