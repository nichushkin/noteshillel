package ua.ithillel.nichushkin.service;

public interface Authentication {
    boolean isUsernamePresent(String username);

    boolean isAuthenticated(String username, String password);
}
